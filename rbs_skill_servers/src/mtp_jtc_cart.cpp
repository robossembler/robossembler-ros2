#include "Eigen/Dense"
#include "control_msgs/action/follow_joint_trajectory.hpp"
#include "kdl/chainfksolverpos_recursive.hpp"
#include "kdl/chainiksolverpos_lma.hpp"
#include "rbs_skill_servers/base_skill.hpp"
#include <Eigen/src/Geometry/Transform.h>
#include <cstddef>
#include <iterator>
#include <kdl/chain.hpp>
#include <kdl/chainiksolver.hpp>
#include <kdl/chainiksolvervel_wdls.hpp>
#include <kdl/chainjnttojacsolver.hpp>
#include <kdl/frames.hpp>
#include <kdl/jntarray.hpp>
#include <kdl/tree.hpp>
#include <kdl_parser/kdl_parser.hpp>
#include <memory>
#include <rbs_skill_interfaces/action/detail/moveit_send_pose__struct.hpp>
#include <rclcpp/logging.hpp>
#include <rclcpp/parameter_client.hpp>
#include <rclcpp_action/client.hpp>
#include <rclcpp_action/client_goal_handle.hpp>
#include <rclcpp_action/create_client.hpp>
#include <sensor_msgs/msg/joint_state.hpp>
#include <string>
#include <tf2_eigen/tf2_eigen.hpp>
#include <tf2_geometry_msgs/tf2_geometry_msgs.hpp>
#include <vector>

namespace rbs_skill_actions {

using MoveitSendPose = rbs_skill_interfaces::action::MoveitSendPose;
using GoalHandleMoveitSendJointStates =
    rclcpp_action::ServerGoalHandle<MoveitSendPose>;
using FollowJointTrajectory = control_msgs::action::FollowJointTrajectory;
using FollowJointTrajectoryGoalHandle =
    rclcpp_action::ClientGoalHandle<FollowJointTrajectory>;

class MoveToPoseJtcCartesian : public SkillBase<MoveitSendPose> {
public:
  explicit MoveToPoseJtcCartesian(
      const rclcpp::NodeOptions &options = rclcpp::NodeOptions())
      : SkillBase<MoveitSendPose>("mtp_jtc_cart", options) {

    m_joint_trajectory_client =
        rclcpp_action::create_client<FollowJointTrajectory>(
            this, "/joint_trajectory_controller/follow_joint_trajectory");

    auto cbg =
        this->create_callback_group(rclcpp::CallbackGroupType::Reentrant);

    rclcpp::SubscriptionOptions s_options;
    s_options.callback_group = cbg;

    m_joint_state_subscriber =
        this->create_subscription<sensor_msgs::msg::JointState>(
            "/joint_states", 10,
            std::bind(&MoveToPoseJtcCartesian::jointStateCallback, this,
                      std::placeholders::_1),
            s_options);
    this->declare_parameter("robot_description", "");
    this->declare_parameter("base_link", "");
    this->declare_parameter("ee_link", "");

    auto robot_description =
        this->get_parameter("robot_description").as_string();
    KDL::Tree kdl_tree;
    if (!kdl_parser::treeFromString(robot_description, kdl_tree)) {
      RCLCPP_ERROR(this->get_logger(),
                   "Failed to obtain KDL tree from the robot description.");

      throw std::runtime_error("KDL Tree initialization failed");
    }

    auto base_link = this->get_parameter("base_link").as_string();
    auto robot_ee_link = this->get_parameter("ee_link").as_string();
    if (base_link.empty() or robot_ee_link.empty()) {
      RCLCPP_ERROR(this->get_logger(),
                   "Describe robot end-effector link and base link to continue");
      throw std::runtime_error("Describe robot end-effector link and base link to continue");
    }

    if (!kdl_tree.getChain(base_link, robot_ee_link, m_kdl_chain)) {
      RCLCPP_ERROR(this->get_logger(),
                   "Failed to obtain KDL chain from base to end-effector.");
      throw std::runtime_error("KDL Chain initialization failed");
    }
  }

protected:
  std::string requiredActionController() override {
    return "joint_trajectory_controller";
  }

  std::vector<std::string> requiredParameters() override { return {"joints"}; }

  void executeAction() override {
    RCLCPP_INFO(this->get_logger(), "Starting executing action goal");

    auto trajectory_goal = FollowJointTrajectory::Goal();
    auto joints = this->get_parameters(requiredParameters());
    m_joint_names = joints.at(0).as_string_array();

    trajectory_goal.trajectory.joint_names = m_joint_names;

    const int max_wait_iterations = 100;
    int wait_count = 0;
    while (m_current_joint_positions.empty() &&
           wait_count++ < max_wait_iterations) {
      std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }
    if (m_current_joint_positions.empty()) {
      RCLCPP_ERROR(this->get_logger(),
                   "Joint positions were not received in time");
      return;
    }

    std::vector<double> target_joint_values;
    Eigen::Affine3d target_pose;
    Eigen::fromMsg(m_current_goal->target_pose, target_pose);
    if (!solveIK(target_pose, target_joint_values)) {
      RCLCPP_ERROR(this->get_logger(),
                   "IK solution not found for goal position");
      m_current_goal_handle->abort(m_current_result);
    }

    trajectory_goal.trajectory.points =
        generate_trajectory(m_current_joint_positions, target_joint_values,
                            m_current_goal->duration);

    auto send_goal_options =
        rclcpp_action::Client<FollowJointTrajectory>::SendGoalOptions();
    send_goal_options.result_callback =
        [this](const FollowJointTrajectoryGoalHandle::WrappedResult
                   &wrapped_result) {
          if (wrapped_result.code == rclcpp_action::ResultCode::SUCCEEDED) {
            RCLCPP_INFO(this->get_logger(), "Goal succeeded");
            m_current_goal_handle->succeed(m_current_result);
          } else {
            RCLCPP_ERROR(this->get_logger(), "Goal failed");
            m_current_goal_handle->abort(m_current_result);
          }
        };

    m_joint_trajectory_client->async_send_goal(trajectory_goal,
                                               send_goal_options);
  }

private:
  void jointStateCallback(const sensor_msgs::msg::JointState::SharedPtr msg) {
    if (m_joint_names.empty()) {
      return;
    }
    if (m_joint_name_to_index.empty()) {
      m_joint_name_to_index.reserve(m_joint_names.size());
      for (size_t j = 0; j < m_joint_names.size(); ++j) {
        auto it =
            std::find(msg->name.begin(), msg->name.end(), m_joint_names[j]);
        if (it != msg->name.end()) {
          size_t index = std::distance(msg->name.begin(), it);
          m_joint_name_to_index[m_joint_names[j]] = index;
        }
      }
    }

    if (m_current_joint_positions.size() != m_joint_names.size()) {
      m_current_joint_positions.resize(m_joint_names.size(), 0.0);
    }

    for (size_t j = 0; j < m_joint_names.size(); ++j) {
      auto index_it = m_joint_name_to_index.find(m_joint_names[j]);
      if (index_it != m_joint_name_to_index.end()) {
        m_current_joint_positions[j] = msg->position[index_it->second];
      }
    }
  }

  std::vector<trajectory_msgs::msg::JointTrajectoryPoint>
  generate_trajectory(const std::vector<double> &start_joint_values,
                      const std::vector<double> &target_joint_values,
                      const double duration) {
    const int num_points = 100;
    std::vector<trajectory_msgs::msg::JointTrajectoryPoint> points;

    KDL::Frame start_pose_kdl, target_pose_kdl;
    if (!getEndEffectorPose(start_joint_values, start_pose_kdl) ||
        !getEndEffectorPose(target_joint_values, target_pose_kdl)) {
      RCLCPP_ERROR(this->get_logger(),
                   "Failed to get initial or target end-effector pose");
      return points;
    }

    Eigen::Affine3d start_pose = KDLFrameToEigen(start_pose_kdl);
    Eigen::Affine3d target_pose = KDLFrameToEigen(target_pose_kdl);

    for (int i = 0; i <= num_points; ++i) {
      trajectory_msgs::msg::JointTrajectoryPoint point;
      double t = static_cast<double>(i) / num_points;

      Eigen::Vector3d start_translation = start_pose.translation();
      Eigen::Vector3d target_translation = target_pose.translation();
      Eigen::Vector3d interpolated_translation =
          (1.0 - t) * start_translation + t * target_translation;
      Eigen::Translation3d interpolated_translation_transform(
          interpolated_translation);

      Eigen::Quaterniond start_quaternion(start_pose.rotation());
      Eigen::Quaterniond target_quaternion(target_pose.rotation());

      Eigen::Quaterniond interpolated_quaternion =
          start_quaternion.slerp(t, target_quaternion);

      Eigen::Affine3d interpolated_pose =
          interpolated_translation_transform * interpolated_quaternion;

      std::vector<double> ik_joint_values;
      if (!solveIK(interpolated_pose, ik_joint_values)) {
        RCLCPP_ERROR(this->get_logger(), "IK solution not found for point %d",
                     i);
        return {};
      }

      point.positions = ik_joint_values;
      point.time_from_start = rclcpp::Duration::from_seconds(t * duration);
      points.push_back(point);
    }
    return points;
  }

  bool getEndEffectorPose(const std::vector<double> &joint_positions,
                          KDL::Frame &end_effector_pose) {

    if (joint_positions.size() != m_joint_names.size()) {
      RCLCPP_ERROR(this->get_logger(), "Mismatch between provided joint "
                                       "positions and expected joint names.");
      RCLCPP_ERROR(this->get_logger(), "Joint pos size: %zu",
                   joint_positions.size());

      RCLCPP_ERROR(this->get_logger(), "Joint names size: %zu",
                   m_joint_names.size());

      return false;
    }

    std::unordered_set<std::string> available_joints(m_joint_names.begin(),
                                                     m_joint_names.end());
    if (available_joints.size() != m_joint_names.size()) {
      RCLCPP_ERROR(this->get_logger(),
                   "Duplicate joints detected in joint names list.");
      return false;
    }

    KDL::JntArray q_in(joint_positions.size());
    for (size_t i = 0; i < joint_positions.size(); ++i) {
      q_in(i) = joint_positions[i];
    }

    KDL::ChainFkSolverPos_recursive fk_solver(m_kdl_chain);
    if (fk_solver.JntToCart(q_in, end_effector_pose) < 0) {
      RCLCPP_ERROR(this->get_logger(),
                   "Failed to compute FK for the specified joint positions.");
      return false;
    }
    return true;
  }

  Eigen::Affine3d KDLFrameToEigen(const KDL::Frame &frame) {
    Eigen::Affine3d transform;
    for (int i = 0; i < 3; ++i) {
      for (int j = 0; j < 3; ++j) {
        transform(i, j) = frame.M(i, j);
      }
      transform(i, 3) = frame.p[i];
    }
    transform(3, 3) = 1.0;
    return transform;
  }

  bool solveIK(const Eigen::Affine3d &target_pose, std::vector<double> &out) {
    KDL::JntArray q_in(m_joint_names.size());
    for (size_t i = 0; i < m_joint_names.size(); ++i) {
      q_in(i) = m_current_joint_positions[i];
    }

    KDL::JntArray q_out(m_joint_names.size());

    KDL::Frame target_pose_kdl(
        KDL::Rotation(target_pose(0, 0), target_pose(0, 1), target_pose(0, 2),
                      target_pose(1, 0), target_pose(1, 1), target_pose(1, 2),
                      target_pose(2, 0), target_pose(2, 1), target_pose(2, 2)),
        KDL::Vector(target_pose.translation().x(),
                    target_pose.translation().y(),
                    target_pose.translation().z()));

    auto ik_solver =
        std::make_unique<KDL::ChainIkSolverPos_LMA>(m_kdl_chain, 1e-5, 500);

    if (ik_solver->CartToJnt(q_in, target_pose_kdl, q_out) >= 0) {
      out.resize(q_out.rows());
      for (size_t i = 0; i < out.size(); i++) {
        out[i] = q_out(i);
      }
      return true;
    } else {
      RCLCPP_ERROR(this->get_logger(), "IK solution not found.");
      return false;
    }
  }

  rclcpp_action::Client<FollowJointTrajectory>::SharedPtr
      m_joint_trajectory_client;
  rclcpp::Subscription<sensor_msgs::msg::JointState>::SharedPtr
      m_joint_state_subscriber;

  std::vector<double> m_current_joint_positions;
  std::unordered_map<std::string, size_t> m_joint_name_to_index;
  std::vector<std::string> m_joint_names;
  KDL::Chain m_kdl_chain;
};

} // namespace rbs_skill_actions

#include "rclcpp_components/register_node_macro.hpp"
RCLCPP_COMPONENTS_REGISTER_NODE(rbs_skill_actions::MoveToPoseJtcCartesian);
