#include "behaviortree_ros2/bt_action_node.hpp"
#include "rbs_skill_interfaces/action/moveit_send_pose.hpp"
#include <behaviortree_cpp/tree_node.h>
#include <behaviortree_ros2/plugins.hpp>
#include <behaviortree_ros2/ros_node_params.hpp>

using namespace BT;
using MoveitSendPoseAction = rbs_skill_interfaces::action::MoveitSendPose;

class MoveToPose : public RosActionNode<MoveitSendPoseAction> {
public:
  MoveToPose(const std::string &name, const NodeConfig &conf,
             const RosNodeParams &params)
      : RosActionNode<MoveitSendPoseAction>(name, conf, params) {

    RCLCPP_INFO(this->logger(), "Starting MoveToPose");
  }

  static BT::PortsList providedPorts() {
    return providedBasicPorts(
        {BT::InputPort<std::string>("robot_name"),
         BT::InputPort<std::shared_ptr<geometry_msgs::msg::Pose>>("pose"),
         BT::InputPort<double>("duration")});
  }

  bool setGoal(RosActionNode::Goal &goal) override {
    RCLCPP_INFO(this->logger(), "Starting send request");
    getInput("robot_name", goal.robot_name);
    getInput("pose", m_target_pose);
    getInput("duration", goal.duration);
    goal.target_pose = *m_target_pose;
    return true;
  }

  NodeStatus onResultReceived(const WrappedResult &wr) override {

    RCLCPP_INFO(this->logger(), "Starting get response");
    if (!wr.result->success) {
      return NodeStatus::FAILURE;
    }
    return NodeStatus::SUCCESS;
  }
private:
  std::shared_ptr<geometry_msgs::msg::Pose> m_target_pose;
};

CreateRosNodePlugin(MoveToPose, "MoveToPose");
