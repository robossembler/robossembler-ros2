from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch_ros.actions import Node
from launch.substitutions import LaunchConfiguration, PathJoinSubstitution
from launch_ros.substitutions import FindPackageShare

def generate_launch_description():

    executor_params = PathJoinSubstitution([FindPackageShare("rbs_bt_executor"), "config", "bt_executor.yaml"])

    nodes_to_start = [
        Node(
            package='rbs_bt_executor',
            executable='rbs_bt_executor',
            # prefix=['gdbserver localhost:1234'],
            parameters=[
                executor_params,
                {'use_sim_time': True}
            ],
        )
    ]

    return LaunchDescription(nodes_to_start)
