#include "rbs_skill_interfaces/srv/get_pick_place_poses.hpp"
#include "rbs_utils/rbs_utils.hpp"
#include "rclcpp/rclcpp.hpp"
#include "rclcpp_components/register_node_macro.hpp"
#include <Eigen/Core>
#include <tf2_eigen/tf2_eigen.hpp>
#include <tf2_ros/transform_listener.h>

namespace rbs_skill_actions {
class GetGraspPickPoseServer : public rclcpp::Node {
public:
  explicit GetGraspPickPoseServer(rclcpp::NodeOptions options);

private:
  static std::vector<geometry_msgs::msg::Pose>
  collectPose(const Eigen::Isometry3d &graspPose,
              const geometry_msgs::msg::Vector3 &move_direction,
              const Eigen::Vector3d &scale_vec);
  rclcpp::Service<rbs_skill_interfaces::srv::GetPickPlacePoses>::SharedPtr srv_;
  std::shared_ptr<tf2_ros::TransformListener> tf_listener_{nullptr};
  std::unique_ptr<tf2_ros::Buffer> tf_buffer_;
  geometry_msgs::msg::TransformStamped place_pose_tf;
  geometry_msgs::msg::TransformStamped grasp_pose_tf;

  // std::vector<geometry_msgs::msg::Pose>
  // getPlacePoseJson(const nlohmann::json &json);
  void handleServer(
      const rbs_skill_interfaces::srv::GetPickPlacePoses::Request::SharedPtr
          request,
      rbs_skill_interfaces::srv::GetPickPlacePoses::Response::SharedPtr
          response);
  std::vector<double> grasp_param_pose;
  Eigen::Affine3d get_Affine_from_arr(const std::vector<double> pose);
};
} // namespace rbs_skill_actions

RCLCPP_COMPONENTS_REGISTER_NODE(rbs_skill_actions::GetGraspPickPoseServer);
