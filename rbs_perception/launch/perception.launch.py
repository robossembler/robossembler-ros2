from launch_ros.actions import Node
from launch import LaunchDescription

def generate_launch_description():
    declared_arguments = []
    
    rbs_interface = Node(
        package="rbs_perception",
        executable="rbs_interface.py",
    )
    pose_estimation = Node(
        package="rbs_perception",
        executable="pose_estimation_lifecycle.py",
    )
    object_detection = Node(
        package="rbs_perception",
        executable="detection_lifecycle.py",
    )

    nodes_to_start = [
        rbs_interface,
        pose_estimation,
        object_detection,
    ]
    return LaunchDescription(declared_arguments + nodes_to_start)
