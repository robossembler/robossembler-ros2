#include "control_msgs/action/follow_joint_trajectory.hpp"
#include "rbs_skill_servers/base_skill.hpp"
#include <memory>
#include <rbs_skill_interfaces/action/detail/moveit_send_joint_states__struct.hpp>
#include <rclcpp/logging.hpp>
#include <rclcpp_action/client.hpp>
#include <rclcpp_action/client_goal_handle.hpp>
#include <rclcpp_action/create_client.hpp>
#include <sensor_msgs/msg/joint_state.hpp>
#include <string>
#include <vector>

namespace rbs_skill_actions {

using MoveitSendJointStates =
    rbs_skill_interfaces::action::MoveitSendJointStates;
using GoalHandleMoveitSendJointStates =
    rclcpp_action::ServerGoalHandle<MoveitSendJointStates>;
using FollowJointTrajectory = control_msgs::action::FollowJointTrajectory;
using FollowJointTrajectoryGoalHandle =
    rclcpp_action::ClientGoalHandle<FollowJointTrajectory>;

class MoveToJointStateActionServer : public SkillBase<MoveitSendJointStates> {
public:
  explicit MoveToJointStateActionServer(
      const rclcpp::NodeOptions &options = rclcpp::NodeOptions())
      : SkillBase<MoveitSendJointStates>("mtjs_jtc", options) {

    m_joint_trajectory_client =
        rclcpp_action::create_client<FollowJointTrajectory>(
            this, "/joint_trajectory_controller/follow_joint_trajectory");

    auto cbg =
        this->create_callback_group(rclcpp::CallbackGroupType::Reentrant);

    rclcpp::SubscriptionOptions s_options;
    s_options.callback_group = cbg;

    m_joint_state_subscriber =
        this->create_subscription<sensor_msgs::msg::JointState>(
            "/joint_states", 10,
            std::bind(&MoveToJointStateActionServer::jointStateCallback, this,
                      std::placeholders::_1),
            s_options);
  }

protected:
  std::string requiredActionController() override {
    return "joint_trajectory_controller";
  }

  std::vector<std::string> requiredParameters() override { return {"joints"}; }

  void executeAction() override {
    RCLCPP_INFO(this->get_logger(), "Starting executing action goal");

    auto trajectory_goal = FollowJointTrajectory::Goal();
    auto joints = this->get_parameters(requiredParameters());
    m_joint_names = joints.at(0).as_string_array();

    trajectory_goal.trajectory.joint_names = m_joint_names;

    const int max_wait_iterations = 100;
    int wait_count = 0;
    while (m_current_joint_positions.empty() &&
           wait_count++ < max_wait_iterations) {
      std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }
    if (m_current_joint_positions.empty()) {
      RCLCPP_ERROR(this->get_logger(),
                   "Joint positions were not received in time");
      return;
    }

    trajectory_goal.trajectory.points = generate_trajectory(
        m_current_joint_positions, m_current_goal->joint_values,
        m_current_goal->duration);

    auto send_goal_options =
        rclcpp_action::Client<FollowJointTrajectory>::SendGoalOptions();
    send_goal_options.result_callback =
        [this](const FollowJointTrajectoryGoalHandle::WrappedResult
                   &wrapped_result) {
          if (wrapped_result.code == rclcpp_action::ResultCode::SUCCEEDED) {
            RCLCPP_INFO(this->get_logger(), "Goal succeeded");
            m_current_goal_handle->succeed(m_current_result);
          } else {
            RCLCPP_ERROR(this->get_logger(), "Goal failed");
            m_current_goal_handle->abort(m_current_result);
          }
        };

    m_joint_trajectory_client->async_send_goal(trajectory_goal,
                                               send_goal_options);
  }

private:
  void jointStateCallback(const sensor_msgs::msg::JointState::SharedPtr msg) {
    if (m_joint_names.empty()) {
      return;
    }
    // RCLCPP_INFO(this->get_logger(), "Called joint positions");

    if (m_joint_mame_to_index.empty()) {
      m_joint_mame_to_index.reserve(m_joint_names.size());
      for (size_t j = 0; j < m_joint_names.size(); ++j) {
        auto it =
            std::find(msg->name.begin(), msg->name.end(), m_joint_names[j]);
        if (it != msg->name.end()) {
          size_t index = std::distance(msg->name.begin(), it);
          m_joint_mame_to_index[m_joint_names[j]] = index;
        }
      }
    }

    if (m_current_joint_positions.size() != m_joint_names.size()) {
      m_current_joint_positions.resize(m_joint_names.size(), 0.0);
    }

    for (size_t j = 0; j < m_joint_names.size(); ++j) {
      auto index_it = m_joint_mame_to_index.find(m_joint_names[j]);
      if (index_it != m_joint_mame_to_index.end()) {
        m_current_joint_positions[j] = msg->position[index_it->second];
      }
    }
  }

  std::vector<trajectory_msgs::msg::JointTrajectoryPoint>
  generate_trajectory(const std::vector<double> &start_joint_values,
                      const std::vector<double> &target_joint_values,
                      const double duration) {

    const int num_points = 100;
    std::vector<trajectory_msgs::msg::JointTrajectoryPoint> points;
    for (int i = 0; i <= num_points; ++i) {
      trajectory_msgs::msg::JointTrajectoryPoint point;
      double t = static_cast<double>(i) / num_points;
      for (size_t j = 0; j < target_joint_values.size(); ++j) {
        double position = start_joint_values[j] +
                          t * (target_joint_values[j] - start_joint_values[j]);
        point.positions.push_back(position);
      }
      point.time_from_start = rclcpp::Duration::from_seconds(t * duration);
      points.push_back(point);
    }
    return points;
  }

  rclcpp_action::Client<FollowJointTrajectory>::SharedPtr
      m_joint_trajectory_client;
  rclcpp::Subscription<sensor_msgs::msg::JointState>::SharedPtr
      m_joint_state_subscriber;

  std::vector<double> m_current_joint_positions;
  std::unordered_map<std::string, size_t> m_joint_mame_to_index;
  std::vector<std::string> m_joint_names;
};

} // namespace rbs_skill_actions

#include "rclcpp_components/register_node_macro.hpp"
RCLCPP_COMPONENTS_REGISTER_NODE(
    rbs_skill_actions::MoveToJointStateActionServer);
