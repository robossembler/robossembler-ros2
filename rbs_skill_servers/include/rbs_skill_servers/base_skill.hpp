#include <algorithm>
#include <controller_manager_msgs/srv/detail/configure_controller__struct.hpp>
#include <controller_manager_msgs/srv/detail/list_controllers__struct.hpp>
#include <controller_manager_msgs/srv/detail/load_controller__struct.hpp>
#include <controller_manager_msgs/srv/detail/switch_controller__struct.hpp>
#include <memory>
#include <rclcpp/callback_group.hpp>
#include <rclcpp/node.hpp>
#include <rclcpp/node_options.hpp>
#include <rclcpp/parameter_client.hpp>
#include <rclcpp_action/create_server.hpp>
#include <rclcpp_action/server.hpp>
#include <rclcpp_action/server_goal_handle.hpp>
#include <rmw/qos_profiles.h>
#include <string>
#include <thread>
#include <vector>

namespace rbs_skill_actions {

template <typename ActionT> class SkillBase : public rclcpp::Node {
public:
  explicit SkillBase(const std::string &node_name,
                     const std::string &action_server_name,
                     const rclcpp::NodeOptions &options)
      : Node(node_name, options) {

    auto act_cbg =
        this->create_callback_group(rclcpp::CallbackGroupType::Reentrant);

    m_action_server = rclcpp_action::create_server<ActionT>(
        this, action_server_name,
        std::bind(&SkillBase::handle_goal, this, std::placeholders::_1,
                  std::placeholders::_2),
        std::bind(&SkillBase::handle_canceled, this, std::placeholders::_1),
        std::bind(&SkillBase::handle_accepted, this, std::placeholders::_1));

    m_load_controller_client =
        this->create_client<controller_manager_msgs::srv::LoadController>(
            "/controller_manager/load_controller");
    m_switch_controller_client =
        create_client<controller_manager_msgs::srv::SwitchController>(
            "/controller_manager/switch_controller");
    m_list_controllers_client =
        create_client<controller_manager_msgs::srv::ListControllers>(
            "/controller_manager/list_controllers");
    m_configure_controller_client =
        create_client<controller_manager_msgs::srv::ConfigureController>(
            "/controller_manager/configure_controller");
  }

  explicit SkillBase(const std::string &node_name,
                     const rclcpp::NodeOptions &options)
      : SkillBase(node_name, node_name, options) {}

protected:
  std::shared_ptr<const typename ActionT::Goal> m_current_goal;
  std::shared_ptr<typename ActionT::Result> m_current_result;
  std::shared_ptr<rclcpp_action::ServerGoalHandle<ActionT>>
      m_current_goal_handle;

  bool m_interfaces_checked{false};

  virtual rclcpp_action::GoalResponse
  handle_goal(const rclcpp_action::GoalUUID &uuid,
              std::shared_ptr<const typename ActionT::Goal> goal) {
    RCLCPP_INFO(this->get_logger(), "Received goal request for robot [%s]",
                goal->robot_name.c_str());
    (void)uuid;

    m_current_goal = goal;
    m_current_result = std::make_shared<typename ActionT::Result>();

    return rclcpp_action::GoalResponse::ACCEPT_AND_EXECUTE;
  }

  virtual rclcpp_action::CancelResponse handle_canceled(
      const std::shared_ptr<rclcpp_action::ServerGoalHandle<ActionT>>
          goal_handle) {
    RCLCPP_INFO(this->get_logger(), "Received request to cancel goal");
    (void)goal_handle;
    return rclcpp_action::CancelResponse::ACCEPT;
  }

  void handle_accepted(
      const std::shared_ptr<rclcpp_action::ServerGoalHandle<ActionT>>
          goal_handle) {
    auto execution_thread = [this, goal_handle]() {
      this->execute(goal_handle);
    };
    std::thread{execution_thread}.detach();
  }

  void execute(const std::shared_ptr<rclcpp_action::ServerGoalHandle<ActionT>>
                   goal_handle) {

    m_current_goal_handle = goal_handle;

    m_required_controllers = requiredStateControllers();
    m_required_controllers.push_back(requiredActionController());
    m_required_controllers.erase(std::remove_if(
        m_required_controllers.begin(), m_required_controllers.end(),
        [](const std::string &str) { return str.empty(); }));

    if (m_required_controllers.empty()) {
      RCLCPP_ERROR(this->get_logger(), "Required controllers is empty");
      m_current_goal_handle->abort(m_current_result);
      return;
    }
    updateControllersMap();

    if (m_controllers_map.count(requiredActionController()) &&
        m_controllers_map[requiredActionController()] == "inactive") {
      switchController(requiredActionController());
    } else if (m_controllers_map.count(requiredActionController()) == 0) {
      loadConfigureSwitchExecuteController({requiredActionController()});
    } else if (!checkParameters()) {
      getRequirementParametersAndExecute(requiredActionController());
    } else {
      executeAction();
    }
  }

  void updateControllersMap() {
    using namespace std::chrono_literals;

    RCLCPP_INFO(this->get_logger(), "Called list_controllers service to check "
                                    "if required controllers are loaded");

    auto list_controllers_request = std::make_shared<
        controller_manager_msgs::srv::ListControllers::Request>();

    auto list_controllers_future =
        m_list_controllers_client->async_send_request(list_controllers_request);

    if (list_controllers_future.wait_for(3s) != std::future_status::ready) {
      RCLCPP_ERROR(this->get_logger(),
                   "Timeout waiting for list_controllers response.");
      return;
    }

    auto response = list_controllers_future.get();
    m_controllers_map.clear();

    std::unordered_set<std::string> loaded_controllers;
    loaded_controllers.reserve(response->controller.size());
    for (const auto &controller : response->controller) {
      m_controllers_map[controller.name] = controller.state;
    }

    // if (m_missing_controllers.empty()) {
    //   RCLCPP_INFO(this->get_logger(), "All required controllers are
    //   loaded."); m_interfaces_checked = true; return true;
    // } else {
    //   RCLCPP_WARN(this->get_logger(), "Missing required controllers:");
    //   for (const auto &missing_controller : m_missing_controllers) {
    //     RCLCPP_WARN(this->get_logger(), "\t- %s",
    //     missing_controller.c_str());
    //   }
    //   m_interfaces_checked = false;
    //   return false;
    // }
  }

  void loadConfigureSwitchExecuteController(
      const std::vector<std::string> &controller_names) {
    for (auto &controller_name : controller_names) {
      RCLCPP_WARN(this->get_logger(), "Attempting to load controller [%s]",
                  controller_name.c_str());
      auto load_request = std::make_shared<
          controller_manager_msgs::srv::LoadController::Request>();
      load_request->name = controller_name;

      auto load_future = m_load_controller_client->async_send_request(
          load_request,
          [this, controller_name](
              rclcpp::Client<controller_manager_msgs::srv::LoadController>::
                  SharedFuture future) {
            if (future.get()->ok) {
              RCLCPP_INFO(this->get_logger(),
                          "Controller %s successfully loaded",
                          controller_name.c_str());
              configureController(controller_name);
            } else {
              RCLCPP_ERROR(this->get_logger(), "Failed to load controller %s",
                           controller_name.c_str());
              m_current_goal_handle->abort(m_current_result);
            }
          });
    }
  }

  void configureController(const std::string &controller_name) {
    auto configure_request = std::make_shared<
        controller_manager_msgs::srv::ConfigureController::Request>();
    configure_request->name = controller_name;

    auto configure_future = m_configure_controller_client->async_send_request(
        configure_request,
        [this, controller_name](
            rclcpp::Client<controller_manager_msgs::srv::ConfigureController>::
                SharedFuture future) {
          if (future.get()->ok) {
            RCLCPP_INFO(this->get_logger(),
                        "Controller %s successfully configured",
                        controller_name.c_str());
            switchController(controller_name);
          } else {
            RCLCPP_ERROR(this->get_logger(),
                         "Failed to configure controller %s",
                         controller_name.c_str());
            m_current_goal_handle->abort(m_current_result);
          }
        });
  }

  void switchController(const std::string &controller_name) {

    detectResourceConflict(controller_name, [this, controller_name](
                                                const std::vector<std::string>
                                                    &conflicted_controllers) {
      auto switch_request = std::make_shared<
          controller_manager_msgs::srv::SwitchController::Request>();
      switch_request->activate_controllers = {controller_name};
      switch_request->deactivate_controllers = conflicted_controllers;
      switch_request->strictness =
          controller_manager_msgs::srv::SwitchController::Request::STRICT;

      auto switch_future = m_switch_controller_client->async_send_request(
          switch_request,
          [this, controller_name](
              rclcpp::Client<controller_manager_msgs::srv::SwitchController>::
                  SharedFuture future) {
            if (future.get()->ok) {
              RCLCPP_INFO(this->get_logger(),
                          "Controller %s successfully switched",
                          controller_name.c_str());
              if (!checkParameters()) {
                getRequirementParametersAndExecute(controller_name);
              } else {
                if (controller_name == requiredActionController()) {
                  executeAction();
                }
              }
            } else {
              RCLCPP_ERROR(this->get_logger(), "Failed to switch controller %s",
                           controller_name.c_str());
              m_current_goal_handle->abort(m_current_result);
            }
          });
    });
  }

  bool checkParameters() {
    std::ostringstream missing_params_stream;
    std::ostringstream empty_params_stream;

    for (const auto &name : requiredParameters()) {
      if (!this->has_parameter(name)) {
        missing_params_stream << "\n\t- " << name;
        continue;
      }

      const auto &parameter = this->get_parameter(name);
      if (isEmptyParameter(parameter)) {
        empty_params_stream << "\n\t- " << name;
      }
    }

    if (missing_params_stream.tellp() > 0) {
      RCLCPP_WARN(this->get_logger(),
                  "The following required parameters are missing:%s",
                  missing_params_stream.str().c_str());
    }

    if (empty_params_stream.tellp() > 0) {
      RCLCPP_WARN(this->get_logger(),
                  "The following required parameters are empty:%s",
                  empty_params_stream.str().c_str());
    }

    return missing_params_stream.tellp() == 0 &&
           empty_params_stream.tellp() == 0;
  }

  bool isEmptyParameter(const rclcpp::Parameter &parameter) {
    switch (parameter.get_type()) {
    case rclcpp::ParameterType::PARAMETER_STRING:
      return parameter.as_string().empty();
    case rclcpp::ParameterType::PARAMETER_STRING_ARRAY:
      return parameter.as_string_array().empty();
    case rclcpp::ParameterType::PARAMETER_INTEGER_ARRAY:
      return parameter.as_integer_array().empty();
    case rclcpp::ParameterType::PARAMETER_DOUBLE_ARRAY:
      return parameter.as_double_array().empty();
    default:
      return false;
    }
  }

  virtual std::string requiredActionController() = 0;

  virtual std::vector<std::string> requiredStateControllers() { return {""}; }

  virtual std::vector<std::string> requiredParameters() { return {}; }

  virtual void executeAction() = 0;

  void getRequirementParametersAndExecute(const std::string &controller_name) {

    RCLCPP_INFO(this->get_logger(),
                "Connected to service %s, asking for parameters:",
                controller_name.c_str());

    for (const auto &param : requiredParameters()) {
      RCLCPP_INFO(this->get_logger(), "\t- %s", param.c_str());
    }

    auto parameter_callback =
        [this, controller_name](
            const std::shared_future<std::vector<rclcpp::Parameter>> &future) {
          try {
            auto parameters = future.get();
            if (parameters.empty()) {
              RCLCPP_ERROR(this->get_logger(),
                           "No parameter found or empty array received");
              m_current_goal_handle->abort(m_current_result);
            } else {
              for (const auto &param : parameters) {
                switch (param.get_type()) {
                case rclcpp::ParameterType::PARAMETER_BOOL:
                  this->declare_parameter(param.get_name(), param.as_bool());
                  break;
                case rclcpp::ParameterType::PARAMETER_INTEGER:
                  this->declare_parameter(param.get_name(), param.as_int());
                  break;
                case rclcpp::ParameterType::PARAMETER_DOUBLE:
                  this->declare_parameter(param.get_name(), param.as_double());
                  break;
                case rclcpp::ParameterType::PARAMETER_STRING:
                  this->declare_parameter(param.get_name(), param.as_string());
                  break;
                case rclcpp::ParameterType::PARAMETER_STRING_ARRAY:
                  this->declare_parameter(param.get_name(),
                                          param.as_string_array());
                  break;
                case rclcpp::ParameterType::PARAMETER_INTEGER_ARRAY:
                  this->declare_parameter(param.get_name(),
                                          param.as_integer_array());
                  break;
                case rclcpp::ParameterType::PARAMETER_DOUBLE_ARRAY:
                  this->declare_parameter(param.get_name(),
                                          param.as_double_array());
                  break;
                default:
                  RCLCPP_WARN(this->get_logger(),
                              "Unsupported parameter type for parameter '%s'",
                              param.get_name().c_str());
                  break;
                }
              }
              if (controller_name == requiredActionController()) {
                executeAction();
              }
            }
          } catch (const std::exception &e) {
            RCLCPP_ERROR(this->get_logger(), "Failed to get parameter: %s",
                         e.what());
          }
        };

    m_parameter_client =
        std::make_shared<rclcpp::AsyncParametersClient>(this, controller_name);

    auto future = m_parameter_client->get_parameters(
        requiredParameters(), std::move(parameter_callback));

    RCLCPP_INFO(this->get_logger(),
                "Asynchronous request sent for required parameters");
  }

  void detectResourceConflict(
      const std::string &new_controller,
      std::function<void(const std::vector<std::string> &)> callback) {

    auto list_controllers_request = std::make_shared<
        controller_manager_msgs::srv::ListControllers::Request>();

    m_list_controllers_client->async_send_request(
        list_controllers_request,
        [this, new_controller,
         callback](const rclcpp::Client<
                   controller_manager_msgs::srv::ListControllers>::SharedFuture
                       future) {
          std::vector<std::string> conflicted_controller;
          auto it_new_controller = std::find_if(
              future.get()->controller.begin(), future.get()->controller.end(),
              [&new_controller](
                  const controller_manager_msgs::msg::ControllerState
                      controller) {
                return controller.name == new_controller;
              });

          if (it_new_controller != future.get()->controller.end()) {
            auto new_controller_interface =
                it_new_controller->required_command_interfaces;
            RCLCPP_INFO(this->get_logger(), "Received list of controllers");

            for (const auto &controller : future.get()->controller) {
              if (controller.state == "active") {
                RCLCPP_INFO(this->get_logger(), "Checking controller: %s",
                            controller.name.c_str());

                bool has_conflict = std::any_of(
                    controller.required_command_interfaces.begin(),
                    controller.required_command_interfaces.end(),
                    [&new_controller_interface](const std::string &interface) {
                      return std::find(new_controller_interface.begin(),
                                       new_controller_interface.end(),
                                       interface) !=
                             new_controller_interface.end();
                    });

                if (has_conflict) {
                  conflicted_controller.push_back(controller.name);
                  RCLCPP_WARN(this->get_logger(), "Conflicted controller: %s",
                              controller.name.c_str());
                }
              }
            }
          } else {
            RCLCPP_WARN(this->get_logger(),
                        "New controller not found in loaded controllers");
          }

          callback(conflicted_controller);
        });
  }

private:
  rclcpp::Client<controller_manager_msgs::srv::LoadController>::SharedPtr
      m_load_controller_client;
  rclcpp::Client<controller_manager_msgs::srv::SwitchController>::SharedPtr
      m_switch_controller_client;
  rclcpp::Client<controller_manager_msgs::srv::ListControllers>::SharedPtr
      m_list_controllers_client;
  rclcpp::Client<controller_manager_msgs::srv::ConfigureController>::SharedPtr
      m_configure_controller_client;
  rclcpp::AsyncParametersClient::SharedPtr m_parameter_client;
  typename rclcpp_action::Server<ActionT>::SharedPtr m_action_server;
  std::vector<std::string> m_required_controllers{};
  std::vector<std::string> m_missing_controllers{};
  std::unordered_map<std::string, std::string> m_controllers_map;
};

} // namespace rbs_skill_actions
