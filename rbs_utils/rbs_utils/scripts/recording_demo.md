### Пример запуска записи демонстрации в RosBag2

Подготовить папку с файлами BT v.4
* Папка /path/to/bt/
* bt.xml
```xml
<?xml version="1.0" encoding="UTF-8"?>
<root BTCPP_format="4">
    <BehaviorTree ID="Main">
        <Sequence>
            <Action ID="RbsAction" do="RecordingDemo" command="rdConfigure" sid="a"></Action>
            <Action ID="RbsAction" do="MoveToPose" command="move" sid="c"></Action>
            <Action ID="RbsAction" do="RecordingDemo" command="rdStop" sid="a"></Action>
        </Sequence>
    </BehaviorTree>
    <TreeNodesModel>
        <Action ID="RbsAction">
            <input_port name="do"/>
            <input_port name="command"/>
	    <input_port name="sid"/>
        </Action>
    </TreeNodesModel>
</root>
```
* skills.json
```json
{
  "skills": [
    {
      "sid": "a",
      "SkillPackage": { "name": "Robossembler", "version": "1.0", "format": "1" },
      "Module": { "name": "RecordingDemo", "description": "Recording a demo via RosBag", "node_name": "lc_record_demo" },
      "Launch": { "package": "rbs_utils", "executable": "recording_demo_via_rosbag.py" },
      "BTAction": [
        {
          "name": "rdConfigure",
          "type_action": "action",
          "type": "run",
          "param": []
        },
        { "name": "rdStop", "type_action": "action", "type": "stop", "param": [] }
      ],
      "Settings": {
          "output": {
              "params": [
                  {
                    "name": "output_path",
                    "value": "rbs_testbag"
                  }
              ]
          }
      }
    },
    {
      "sid": "c",
      "SkillPackage": { "name": "Robossembler", "version": "1.0", "format": "1" },
      "Module": { "node_name": "skill_mtp", "name": "MoveToPose", "description": "Move to pose with cartesian controller" },
      "Launch": { "executable": "skill_movetopose.py", "package": "rbss_movetopose" },
      "BTAction": [
        {
            "name": "move",
            "type_action": "action",
            "type": "action",
            "param": [
              {
                "type": "move_to_pose",
                "dependency": { "robot_name": "arm0",
                                "pose": { "position": {"x":0.1, "y":0.1, "z":0.9}, "orientation": {"x":0.55, "y":0.0, "z":0.45, "w": 1.0} } }
              }
            ]
        }
      ],
      "topicsOut": [],
      "Settings": {
          "output": {
              "params": [
                  {
                      "name": "server_name",
                      "value": "mtp_cart"
                  },
                  {
                      "name": "duration",
                      "value": "5.0"
                  },
                  {
                      "name": "end_effector_velocity",
                      "value": "1.0"
                  },
                  {
                      "name": "end_effector_acceleration",
                      "value": "1.0"
                  }
              ]
          }
      }
    }
  ]
}
```

Запустить последовательно три терминала и выполнить запуск 3-х launch-файлов:
* 1 - запуск интерфейсной ноды с серверами навыков
```bash
ros2 launch rbs_bt_executor interface.launch.py bt_path:=/path/to/bt
```
* 2 - запуск робота в сцене
```bash
ros2 launch rbs_bringup rbs_bringup.launch.py
```
* 3 - запуск дерева поведения
```bash
ros2 launch rbs_bt_executor rbs_bt_web.launch.py bt_path:=/path/to/bt
```
