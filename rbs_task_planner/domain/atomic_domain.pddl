(define (domain atomic_domain)
    (:requirements :strips :typing :adl :fluents :durative-actions)
    (:types
        zone
        part
        arm
        assembly
    )

    (:predicates
        (arm_available ?a - arm)
        (assembly_at ?a - assembly ?z - zone)
        (part_of ?part - part ?whole - assembly)
        (assembled ?whole - assembly)
        (assembly_order ?prev ?next - assembly)
    )

    (:durative-action assemble
        :parameters (?p - part ?prev ?next - assembly ?z - zone ?a - arm)
        :duration ( = ?duration 1)
        :condition (and 
            (at start (arm_available ?a))
            (at start (assembly_order ?prev ?next))
            (at start (part_of ?p ?next))
            (at start (assembly_at ?prev ?z))
            (at start (assembled ?prev))
        )
        :effect (and
            (at start (not (arm_available ?a)))
            (at end (assembly_at ?next ?z))
            (at end (assembled ?next))
            (at end (arm_available ?a))
        )
    )
)
