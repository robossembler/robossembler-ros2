#include "rbs_utils/rbs_utils.hpp"
#include <Eigen/src/Geometry/Transform.h>
#include <dynmsg/typesupport.hpp>
#include <fstream>
#include <geometry_msgs/msg/detail/pose__struct.hpp>
#include <geometry_msgs/msg/detail/transform_stamped__struct.hpp>
#include <rbs_utils_interfaces/msg/detail/assembly_config__struct.hpp>
#include <rbs_utils_interfaces/msg/detail/assembly_config__traits.hpp>
#include <rclcpp/logging.hpp>
#include <rclcpp/node_interfaces/node_clock_interface.hpp>
#include <string>
#include <strstream>
#include <tf2/LinearMath/Transform.h>
#include <tf2/convert.h>
#include <tf2_eigen/tf2_eigen.hpp>
#include <tf2_geometry_msgs/tf2_geometry_msgs.hpp>
#include <tf2_msgs/msg/detail/tf_message__struct.hpp>
#include <tf2_ros/buffer_interface.h>
#include <yaml-cpp/emitter.h>
#include <yaml-cpp/node/parse.h>

namespace rbs_utils {
AssemblyConfigLoader::AssemblyConfigLoader(
    const std::string &t_assembly_dir,
    const rclcpp::node_interfaces::NodeLoggingInterface::SharedPtr
        &t_logging_interface,
    const rclcpp::node_interfaces::NodeClockInterface::SharedPtr
        &t_clock_interface)
    : m_assembly_dir(t_assembly_dir),
      m_logger(t_logging_interface->get_logger()),
      m_clock(t_clock_interface->get_clock()) {
  if (!m_assembly_dir.empty()) {
    std::vector<std::string> filenames = {"rbs_db"};
    for (auto &filename : filenames) {
      std::string filepath =
          env_dir + "/" + m_assembly_dir + "/" + filename + ".yaml";

      m_env_files.push_back(filepath);
      parseRbsDb(filepath);
    }
  } else {
    RCLCPP_ERROR(m_logger, "Assembly dir is not set");
  }
}

void AssemblyConfigLoader::parseRbsDb(const std::string &filepath) {
  try {
    YAML::Node asm_config = YAML::LoadFile(filepath);
    std::string asm_config_string = dynmsg::yaml_to_string(asm_config);

    RosMessage_Cpp rosmsg;
    InterfaceTypeName interface{"rbs_utils_interfaces", "AssemblyConfig"};
    rosmsg.type_info = dynmsg::cpp::get_type_info(interface);

    void *ros_msg = reinterpret_cast<void *>(&m_assembly_config);
    dynmsg::cpp::yaml_and_typeinfo_to_rosmsg(rosmsg.type_info,
                                             asm_config_string, ros_msg);

  } catch (const std::exception &e) {
    RCLCPP_ERROR(m_logger, "Exception reading file %s: %s", filepath.c_str(),
                 e.what());
  }
}

[[deprecated("Not Implemented")]] void AssemblyConfigLoader::saveRbsConfig() {}

std::vector<std::string> AssemblyConfigLoader::getUniqueSceneModelNames() {
  std::vector<std::string> model_names;
  if (m_assembly_config.relative_part.size() != 0) {
    for (auto &t : m_assembly_config.relative_part) {
      model_names.push_back(t.name);
    }
  } else {
    return model_names;
  }
  // Sort and remove duplicates
  std::sort(model_names.begin(), model_names.end());
  model_names.erase(std::unique(model_names.begin(), model_names.end()),
                    model_names.end());

  return model_names;
}

tf2_msgs::msg::TFMessage AssemblyConfigLoader::getAllPossibleTfData() {
  tf2_msgs::msg::TFMessage tp;
  // Get absolute parts
  for (auto &abs_poses : m_assembly_config.absolute_part) {
    geometry_msgs::msg::TransformStamped tmp;
    tmp.transform = createTransform(abs_poses.pose);
    tmp.child_frame_id = abs_poses.name;
    tmp.header.frame_id = "world";
    tmp.header.stamp = m_clock->now();
    tp.transforms.push_back(tmp);
  }
  // Get relative parts
  for (const auto &relative_part : m_assembly_config.relative_part) {
    geometry_msgs::msg::TransformStamped tmp;
    tmp.transform = createTransform(relative_part.pose);
    tmp.child_frame_id = relative_part.name;
    tmp.header.frame_id = relative_part.relative_at;
    tmp.header.stamp = m_clock->now();
    tp.transforms.push_back(tmp);
    RCLCPP_INFO(m_logger, "Model name [%s]", relative_part.name.c_str());
  }
  // Get grasp poses
  for (const auto &grasp_pose : m_assembly_config.grasp_poses) {
    geometry_msgs::msg::TransformStamped tmp;
    tmp.transform = createTransform(grasp_pose.pose);
    tmp.child_frame_id = grasp_pose.name;
    tmp.header.frame_id = grasp_pose.relative_at;
    tmp.header.stamp = m_clock->now();
    tp.transforms.push_back(tmp);
  }

  return tp;
}

tf2_msgs::msg::TFMessage
AssemblyConfigLoader::getGraspTfData(const std::string &model_name) {
  tf2_msgs::msg::TFMessage tp;

  bool found_grasp_pose = false;
  if (!m_assembly_config.relative_part.empty()) {
    for (auto &abs_poses : m_assembly_config.absolute_part) {
      geometry_msgs::msg::TransformStamped tmp;
      tmp.transform = createTransform(abs_poses.pose);
      tmp.child_frame_id = abs_poses.name;
      tmp.header.frame_id = "world";
      tmp.header.stamp = m_clock->now();
      tp.transforms.push_back(tmp);
    }
  } else {
    RCLCPP_ERROR(m_logger, "Relative parts is empty size: %zu",
                 m_assembly_config.relative_part.size());
  }

  for (const auto &grasp_pose : m_assembly_config.grasp_poses) {
    if (grasp_pose.relative_at == model_name) {
      geometry_msgs::msg::TransformStamped tmp;
      tmp.transform = createTransform(grasp_pose.pose);
      tmp.child_frame_id = grasp_pose.name;
      tmp.header.frame_id = grasp_pose.relative_at;
      tmp.header.stamp = m_clock->now();
      tp.transforms.push_back(tmp);
      found_grasp_pose = true;
    }
  }

  if (!found_grasp_pose) {
    RCLCPP_ERROR(m_logger, "Grasp pose not found for model %s",
                 model_name.c_str());
  }

  return tp;

}

tf2_msgs::msg::TFMessage
AssemblyConfigLoader::getTfData(const std::string &model_name) {
  tf2_msgs::msg::TFMessage tp;

  if (!m_assembly_config.absolute_part.empty()) {
    for (auto &abs_part : m_assembly_config.absolute_part) {
      geometry_msgs::msg::TransformStamped abs_transrorm_stamped;
      abs_transrorm_stamped.transform = createTransform(abs_part.pose);
      abs_transrorm_stamped.child_frame_id = abs_part.name;
      abs_transrorm_stamped.header.frame_id = "world";
      abs_transrorm_stamped.header.stamp = m_clock->now();
      tp.transforms.push_back(abs_transrorm_stamped);
    }
  } else {
    RCLCPP_ERROR(m_logger, "Absolute parts is empty size: %zu",
                 m_assembly_config.absolute_part.size());
  }

  bool found_model = false;
  bool found_grasp_pose = false;
  if (!m_assembly_config.relative_part.empty()) {
    for (auto &abs_poses : m_assembly_config.absolute_part) {
      geometry_msgs::msg::TransformStamped tmp;
      tmp.transform = createTransform(abs_poses.pose);
      tmp.child_frame_id = abs_poses.name;
      tmp.header.frame_id = "world";
      tmp.header.stamp = m_clock->now();
      tp.transforms.push_back(tmp);
    }

    for (const auto &relative_part : m_assembly_config.relative_part) {
      // Find our model data
      if (relative_part.name == model_name) {
        geometry_msgs::msg::TransformStamped tmp;
        tmp.transform = createTransform(relative_part.pose);
        tmp.child_frame_id = relative_part.name;
        tmp.header.frame_id = relative_part.relative_at;
        tmp.header.stamp = m_clock->now();
        tp.transforms.push_back(tmp);
        found_model = true;
      }
      RCLCPP_INFO(m_logger, "Model name [%s]",
                  relative_part.relative_at.c_str());
    }
  } else {
    RCLCPP_ERROR(m_logger, "Relative parts is empty size: %zu",
                 m_assembly_config.relative_part.size());
  }

  for (const auto &grasp_pose : m_assembly_config.grasp_poses) {
    if (grasp_pose.relative_at == model_name) {
      geometry_msgs::msg::TransformStamped tmp;
      tmp.transform = createTransform(grasp_pose.pose);
      tmp.child_frame_id = grasp_pose.name;
      tmp.header.frame_id = grasp_pose.relative_at;
      tmp.header.stamp = m_clock->now();
      tp.transforms.push_back(tmp);
      found_grasp_pose = true;
    }
  }

  if (!found_model) {
    RCLCPP_ERROR(m_logger, "Model %s not found in config", model_name.c_str());
  }
  if (!found_grasp_pose) {
    RCLCPP_ERROR(m_logger, "Grasp pose not found for model %s",
                 model_name.c_str());
  }

  return tp;
}

geometry_msgs::msg::Transform
AssemblyConfigLoader::createTransform(const geometry_msgs::msg::Pose &pose) {
  geometry_msgs::msg::Transform transform;
  transform.translation.x = pose.position.x;
  transform.translation.y = pose.position.y;
  transform.translation.z = pose.position.z;
  transform.rotation.x = pose.orientation.x;
  transform.rotation.y = pose.orientation.y;
  transform.rotation.z = pose.orientation.z;
  transform.rotation.w = pose.orientation.w;
  return transform;
}

geometry_msgs::msg::PoseArray AssemblyConfigLoader::getWorkspace() {
  geometry_msgs::msg::PoseArray pose_array;
  pose_array.header.frame_id = "world";

  if (m_assembly_config.workspace.empty()) {
    RCLCPP_WARN(m_logger, "Workspace is empty, check your robossembler_db");
    return pose_array;
  }

  pose_array.poses.reserve(m_assembly_config.workspace.size());

  const double default_rotation_value = 0.0;
  const double default_rotation_w = 1.0;

  for (const auto &point : m_assembly_config.workspace) {
    geometry_msgs::msg::Pose pose;
    pose.position.x = point.x;
    pose.position.y = point.y;
    pose.position.z = point.z;
    pose.orientation.x = default_rotation_value;
    pose.orientation.y = default_rotation_value;
    pose.orientation.z = default_rotation_value;
    pose.orientation.w = default_rotation_w;
    pose_array.poses.push_back(pose);
  }

  return pose_array;
}

geometry_msgs::msg::PoseArray
AssemblyConfigLoader::getWorkspaceInspectorTrajectory() {
  geometry_msgs::msg::PoseArray pose_array;
  pose_array.header.frame_id = "world";

  auto workspace_poses = getWorkspace();

  for (const auto &pose : workspace_poses.poses) {
    pose_array.poses.push_back(transformTrajectory(pose));
  }

  return pose_array;
}

geometry_msgs::msg::Pose AssemblyConfigLoader::transformTrajectory(
    const geometry_msgs::msg::Pose &pose) {
  Eigen::Isometry3d pose_eigen;
  tf2::fromMsg(pose, pose_eigen);

  Eigen::AngleAxisd rotation(M_PI, Eigen::Vector3d::UnitY());
  pose_eigen.rotate(rotation);
  pose_eigen.translation().z() += 0.35;
  return tf2::toMsg(pose_eigen);
}

} // namespace rbs_utils
