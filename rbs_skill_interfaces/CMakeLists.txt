cmake_minimum_required(VERSION 3.8)
project(rbs_skill_interfaces)

if(CMAKE_COMPILER_IS_GNUCXX OR CMAKE_CXX_COMPILER_ID MATCHES "Clang")
  add_compile_options(-Wall -Wextra -Wpedantic)
endif()

# find dependencies
find_package(ament_cmake REQUIRED)
find_package(rosidl_default_generators REQUIRED)
find_package(geometry_msgs REQUIRED)
find_package(std_msgs REQUIRED)
find_package(moveit_msgs REQUIRED)
find_package(shape_msgs REQUIRED)
# uncomment the following section in order to fill in
# further dependencies manually.
# find_package(<dependency> REQUIRED)

rosidl_generate_interfaces(${PROJECT_NAME}
    "action/MoveitSendPose.action"
    "action/MoveitSendJointStates.action"
    "action/GripperCommand.action"
    "action/RbsBt.action"
    "msg/ObjectInfo.msg"
    "msg/PropertyValuePair.msg"
    "msg/ActionFeedbackStatusConstants.msg"
    "msg/ActionResultStatusConstants.msg"
    "msg/BoundBox.msg"
    "srv/DetectObject.srv"
    "srv/BtInit.srv"
    "srv/AssembleState.srv"
    "srv/GetPickPlacePoses.srv"
    "srv/AddPlanningSceneObject.srv"
    "srv/PlanningSceneObjectState.srv"
    "srv/RbsBt.srv"
    DEPENDENCIES std_msgs geometry_msgs moveit_msgs shape_msgs
)


if(BUILD_TESTING)
  find_package(ament_lint_auto REQUIRED)
  # the following line skips the linter which checks for copyrights
  # comment the line when a copyright and license is added to all source files
  set(ament_cmake_copyright_FOUND TRUE)
  # the following line skips cpplint (only works in a git repo)
  # comment the line when this package is in a git repo and when
  # a copyright and license is added to all source files
  set(ament_cmake_cpplint_FOUND TRUE)
  ament_lint_auto_find_test_dependencies()
endif()

ament_package()
